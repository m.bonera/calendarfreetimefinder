#!/usr/bin/python

#person1cal = [['9:00','10:30'],['12:00','13:00'],['16:00','17:00'],['18:30','19:00']]
person1cal = [['9:30','10:30']]
person1wt = ['9:00','20:00']

#person2cal = [['10:00','11:30'],['12:30','14:30'],['16:00','17:00']]
person2cal = [['10:00','11:30'],['16:00','17:00']]
person2wt = ['10:00','19:00']

meeting_time = 145
freetime = []

def convertTime(mtime):
    hour, minute = mtime.split(":")
    return int(hour) * 60 + int(minute)

def convertMinute(mtime):
    hours = mtime // 60
    minutes = mtime % 60 
    return hours,minutes

# Interpolating the beginning and end of meeting time
calStart = person1wt[0] if person1wt[0] > person2wt[0] else person2wt[0]
calStop = person1wt[1] if person1wt[1] < person2wt[1] else person2wt[1]

# Set freetime array to the maximum meeting time
freetime.append([convertTime(calStart),convertTime(calStop)])

# Cycle through person 1 and 2 calendars
for cur_sched in person1cal + person2cal:
    busyStart = convertTime(cur_sched[0])
    busyEnd = convertTime(cur_sched[1])

    # Check if we are in the middle of a free time
    for id, cur_free in enumerate(freetime):

        if busyStart >= cur_free[0] and busyEnd <= cur_free[1]:

            # we are in the middle of an element: we split it 
            newEnd = busyStart
            addStart = busyEnd
            addEnd = cur_free[1]
            # Check if the element is just to be deleted or updated
            if cur_free[0] == newEnd:
                freetime.pop(id)
            # Remove free slot if less than meeting_time
            elif newEnd - cur_free[0] < meeting_time:
                freetime.pop(id)
            else:
                cur_free[1] = newEnd
            
            # Add the second part
            if addEnd - addStart >= meeting_time:
                freetime.insert(id+1,[addStart,addEnd])
                # print(freetime)
                notInside = False
            
            break
        
        # Appointment start is inside a free time slot
        if busyStart > cur_free[0] and busyStart < cur_free[1]:
            # Do nothing for now cur_free[]
            cur_free[1] = busyStart

        # Appointment end is inside a free time slot
        if busyEnd > cur_free[0] and busyEnd < cur_free[1]:
            cur_free[0] = busyEnd

        # Appointment is across a free time slot
        if busyStart < cur_free[0] and busyEnd > cur_free[1]:
            freetime.pop(id)
        
        # Time slot is smaller than meeting time
        if cur_free[1] - cur_free[0] < meeting_time:
            freetime.pop(id)



   
print("Free slots:")  
for id, cur_free in enumerate(freetime):
    slotStart = str(convertMinute(cur_free[0])[0]).zfill(2) + ":" + str(convertMinute(cur_free[0])[1]).zfill(2)
    slotEnd = str(convertMinute(cur_free[1])[0]).zfill(2) + ":" + str(convertMinute(cur_free[1])[1]).zfill(2)
    print(str(id)+") " + slotStart + " - " + slotEnd)
    


