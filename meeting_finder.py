#!/usr/bin/python

#person1cal = [['9:00','10:30'],['12:00','13:00'],['16:00','17:00'],['18:30','19:00']]
person1cal = [['9:30','10:30']]
person1wt = ['9:00','20:00']

#person2cal = [['10:00','11:30'],['12:30','14:30'],['16:00','17:00']]
person2cal = [['10:00','11:30'],['16:00','17:00']]
person2wt = ['10:00','19:00']

meeting_time = 30
freetime = []

def convertTime(mtime):
    hour, minute = mtime.split(":")
    return int(hour) * 60 + int(minute)

def convertMinute(mtime):
    hours = mtime // 60
    minutes = mtime % 60 
    return hours,minutes

def displayTime(mtime):
    hours,minutes = convertMinute(mtime)
    hours = str(hours).zfill(2)
    minutes = str(minutes).zfill(2)
    return hours + ":" + minutes

# Interpolating the beginning and end of meeting time
calStart = person1wt[0] if person1wt[0] > person2wt[0] else person2wt[0]
calStop = person1wt[1] if person1wt[1] < person2wt[1] else person2wt[1]

print("Start time: " + displayTime(calStart))
print("Stop time: " + displayTime(calStop))

# Set freetime array to the maximum meeting time
freetime.append([convertTime(calStart),convertTime(calStop)])

# Cycle through person 1 and 2 calendars
for cur_sched in person1cal + person2cal:
    busyStart = convertTime(cur_sched[0])
    busyEnd = convertTime(cur_sched[1])
    print("Processing appointment that start at " + cur_sched[0] + " and end at " + cur_sched[1])

    # Check if we are in the middle of a free time
    for id, cur_free in enumerate(freetime):

        #print("Processing freetime = ["+str(id)+"] ",cur_free[0],cur_free[1])
        print("Processing free time slot = [" + str(id)+ "] " + displayTime(cur_free[0]) + " - " + displayTime(cur_free[1]))
        
        if busyStart >= cur_free[0] and busyEnd <= cur_free[1]:

            print("Appointment is inside the freetime")
            # we are in the middle of an element: we split it 
            newEnd = busyStart
            addStart = busyEnd
            addEnd = cur_free[1]
            # Check if the element is just to be deleted or updated
            if cur_free[0] == newEnd:
                freetime.pop(id)
                print("Freetime popped out because beginning is equal to appointment start")
            # Remove free slot if less than meeting_time
            elif newEnd - cur_free[0] < meeting_time:
                freetime.pop(id)
                print("Freetime popped out because the duration is less than meeting_time")
            else:
                cur_free[1] = newEnd
                print("Freetime updated: new end is "+displayTime(newEnd))
            
            # Add the second part
            if addEnd - addStart >= meeting_time:
                freetime.insert(id+1,[addStart,addEnd])
                print("Added a new freetime from " + displayTime(addStart) + " to " + displayTime(addEnd))
                # print(freetime)
                notInside = False
            else:
                print("Not added new freetime, shorter than meeting_time")
            
            break
        
        # Appointment start is inside a free time slot
        if busyStart > cur_free[0] and busyStart < cur_free[1]:
            # Do nothing for now cur_free[]
            print("Updated end of free slot from " + displayTime(cur_free[1]) + " to " + displayTime(busyStart))
            cur_free[1] = busyStart

        # Appointment end is inside a free time slot
        if busyEnd > cur_free[0] and busyEnd < cur_free[1]:
            print("Updated beginning of free slot from " + displayTime(cur_free[0]) + " to " + displayTime(busyEnd))
            cur_free[0] = busyEnd

        # Appointment is across a free time slot
        if busyStart < cur_free[0] and busyEnd > cur_free[1]:
            print("Removed free slot that was across appointment from " + displayTime(cur_free[0]) + " to " + displayTime(cur_free[1]))
            freetime.pop(id)
        
        # Time slot is smaller than meeting time
        if cur_free[1] - cur_free[0] < meeting_time:
            print("Removed free slot that was shorter than meeting time - from " + displayTime(cur_free[0]) + " to " + displayTime(cur_free[1]))
            freetime.pop(id)

        # print(freetime)

    print("")  

   
print("Free slots:")  
for id, cur_free in enumerate(freetime):
    slotStart = displayTime(cur_free[0])
    slotEnd = displayTime(cur_free[1])
    print(str(id)+") " + slotStart + " - " + slotEnd)
    


